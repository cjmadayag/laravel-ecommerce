@extends("layouts.app")
@section("content")
<h1 class="text-center">Catalog</h1>

@if(Session::has("message"))
	<h4>{{Session::get("message")}}</h4>
@endif

<div class="container">
	<div class="row">
		@foreach($items as $item)
			<div class="col-lg-4 py-3">
				<div class="card">
					<img src="{{$item->imgPath}}" class="card-img-top" height="300px" alt>
					<div class="card-body text-center">
						<h4 class="card-title">{{$item->name}}</h4>
						<p class="card-text">{{$item->price}}</p>
						<p class="card-text">{{$item->description}}</p>
						<p class="card-text">{{$item->category->name}}</p>
					</div>
					<div class="card-footer text-center d-flex justify-content-center align-items-center">
						<form action="/catalog/{{$item->id}}" method="post">
							@csrf
							@method("DELETE")
							<button class="btn btn-danger">Delete</button>
						</form>
						<a href="/edititem/{{$item->id}}" class="btn btn-secondary">Edit</a>
					</div>
					<div class="card-footer">
						<form method="post" action="/addtocart/{{$item->id}}">
							@csrf
							<input type="number" name="quantity" class="form-control" value="1">
							<button class="btn btn-primary" type="submit">Add to Cart</button>
						</form>
					</div>
				</div>
			</div>

		@endforeach
	</div>
</div>


@endsection