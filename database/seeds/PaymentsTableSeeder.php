<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("payments")->insert(array(
        	0=>
        	array(
        		"id" => 1,
        		"name" => "Cash On Delivery",
                "created_at" => null,
                "updated_at" => null
        	),
            1=>
            array(
                "id" => 2,
                "name" => "Paypal",
                "created_at" => null,
                "updated_at" => null
            ),
            2=>
            array(
                "id" => 3,
                "name" => "Stripe",
                "created_at" => null,
                "updated_at" => null
            )
        ));
    }
}
