<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	DB::table("items")->insert(array(
     		0=>array(
     			"id"=> 1,
     			"name"=> "item1",
     			"price"=> 500.00,
     			"description"=>"description1",
     			"imgPath"=>"imgPath",
     			"category_id"=>1,
     			"created_at"=>null,
     			"updated_at"=>null
     		),
     		1=>array(
     			"id"=> 2,
     			"name"=> "item2",
     			"price"=> 500.00,
     			"description"=>"description2",
     			"imgPath"=>"imgPath",
     			"category_id"=>2,
     			"create_at"=>null,
     			"updated_at"=>null
     		)

     	));
    }
}
