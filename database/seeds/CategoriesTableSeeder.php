<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("categories")->insert(array(
        	0=>
        	array(
        		"id" => 1,
        		"name" => "Star Wars",
                "created_at" => null,
                "updated_at" => null
        	),
            1=>
            array(
                "id" => 2,
                "name" => "Pokemon",
                "created_at" => null,
                "updated_at" => null
            ),
            2=>
            array(
                "id" => 3,
                "name" => "Jollibee",
                "created_at" => null,
                "updated_at" => null
            )
        ));
    }
}
