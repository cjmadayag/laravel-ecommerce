<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//catalog
Route::get("/catalog", "ItemController@index");
Route::delete("/catalog/{id}", "ItemController@destroy");

//ADMINVIEWS
//to additem
Route::get("/additem", "ItemController@create");
//Add Item
Route::post("/additem", "ItemController@store");
//to edititem
Route::get("/edititem/{id}", "ItemController@edit");
//Edit item
Route::patch("/edititem/{id}", "ItemController@update");

//Cart Crud
Route::post("/addtocart/{id}", "ItemController@addToCart");
//Show Cart
Route::get("/cart","ItemController@showCart");